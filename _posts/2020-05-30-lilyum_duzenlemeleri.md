---
title: Lilyum Düzenlemeleri
layout: post
---

<span class="image featured"><img src="{{ site.baseurl }}/assets/images/pic01.jpg" alt=""></span>
</br>


Lilyum, Kiwi ile derlenir ve Lilyum ile alakalı özelleştirmeler bu
yapının izin verdiği gibi iki şekilde gerçekleştirilir.

1- Root, Config.sh ve Config.xml dosyaları ile

2- Proje kapsamında kullanılan Home:Tarbetu OBS deposu aracılığıyla

Birinci maddede bahsi geçen Root, Config.sh ve Config.xml dosyaları
Kiwi'nin çalışma yönergelerini içerir. Bu üç dosya, Lilyum'un
yapılandırılmasında için şu kolaylıkları sağlar:

Config.xml, Lilyum'un içerdiği paketleri ve depoları listeler. Bu
dosyanın "pykiwibuilder" betikleriyle yapılandırılması şiddetle tavsiye
edilir.

Config.sh, Lilyum'un derlenmesi esnasında çalıştırılan komutları içerir.
Elle yapılan derlemeler ve Flatpak komutları bu dosyada belirtilmelidir.

Root klasörü, Lilyum dosya hiyerarşisi içerisinde olması gereken
dosyaları içerir. Başka bir yol olmaksızın yapılabilecek iyileştirmeler
bu yolla yapılmalıdır.

Home:Tarbetu OBS deposu, Lilyum için gerekli paketleri içerir. Bu
deponun kaynak rpm paketleri "Lilyum Source RPM" isimli Gitlab deposunda
saklanır ve bu depodan OBS deposuna yönlendirilir.

İnsanlarda genel olarak kabul görmüş bir galat-ı meşhur, RPM
dosyalarının yalnızca program kurulumu için gerekli olduğudur. RPM
dosyaları aynı şekilde bir dağıtımın yapılandırılması için de gerekli
dosyaları barındırır ve Root dosyası içerisinde yapılan pek çok
değişiklik RPM paketi olarak da sisteme ilave edilebilir.

Tek tek ve dağınık bir halde duran dosyalar yerine RPM paketleriyle
çalışmak daha sistematik bir çalışma avantajı sağlar. Üstelik işi yapan
kişinin sınırlarını da kolaylıkla belirler. Ayrıca yapılması gereken
ufak bir değişiklik kurulacak sistemlere değil zaten hâlihazırda
kurulmuş sistemlere de kolaylıkla eklenebilir.

Bir Lilyum geliştiricisinin kusuru yüzünden sistemlerin önemli bir hata
verdiğini düşünelim. Eğer RPM paketleriyle çalışırsak bu hatayı kimse
fark etmeden kolaylıkla düzeltebiliriz ancak Root dosyası içerisine
yaptığımız eklemelerle işi geçiştirirsek bu önemli hatayı hiçbir şekilde
uzaktan düzeltemeyiz. Hâliyle bizim için ciddi bir prestij kaybı, karşı
taraf için hatanın dozuna göre büyüyen zararlar anlamına gelecektir. RPM
derlemek pratik bir yol olmayabilir ancak kullanıcının sorumluğunu
taşıdığımızı düşünürsek elimizden gelenin en iyisini yapmamız gerekir.

RPM derlemek için bence en pratik yol, en başından paketi derlemek
yerine zaten hâlihazırda kullanılan örneklerini yorumlayarak tekrar
kullanıma sunmaktır.

Bir RPM dosyası, kabaca iki kısımdan oluşur:

-Derlenen arşiv dosyası

-Arşivin nasıl derlenmesi gerektiğiyle alakalı direktifleri içeren SPEC
dosyası.

SPEC dosyası bir RPM paketinin işleyişini temsil eder. Bu dosyanın
yalnızca apaçık belli olan kısımlarını düzenleyip arşiv dosyasını
düzenleyerek Lilyum için gerekli düzeltmeleri gayet güzelce ilave
edilebilir.

Lilyum için yapılan eklemeler ve düzenlemeler şunlardır:

Not: Paketlenmiş dosyalar paket ismiyle anılacaktır.

Calamares - Lilyum

Lilyum'un kurulumunu sağlayan Calamares programının yönergelerini ve
temasını içerir.

BreezeBlurred

Lilyum KDE için varsayılan pencere dekorasyonunu içerir.

Flat-remix-icon-theme

Lilyum'un varsayılan ikon setini içerir.

/boot/grub2/themes/Lilyum/

Lilyum'un Grub temasını içerir. grub2-branding-openSUSE paketi
incelenerek uygun şekilde paketlenebilir. Aynı şekilde,
/usr/share/grub2/themes/Lilyum klasörü altında da aynı dosyalara
ulaşabilirsiniz.

/etc

Bu dizin altındaki dosyaların belirtilmiş olanları dışında pek çoğu
GeckoLinux'tan alınmıştır. Lilyum için gerekli yapılandırmaları içerir.
Bu klasör altında paketlenmesi gereken dosyalar şu şekildedir:

issue (openSUSE-Release paketiyle ilişkilidir)

skel/ (Bir paketle doğrudan ilişkili olmamak dışında, yeni kullanıcılar
için geçerli ayarlamaları içerir. Bu klasör altındaki dosyalar kullanıcı
dosyaları olarak incelenecektir)

/usr/lib/sddm/sddm.conf.d/autologin

SDDM için otomatik giriş özelliği sunar.

/usr/lib/os-release

Dağıtımın Lilyum olduğunu bildirir. (Bu dosya openSUSE-Release paketiyle
ilişkilidir.)

/usr/share/plasma/look-and-feel/Lilyum/

KDE Masaüstü için gerekli tema ayarlarını içerir.

/usr/share/sddm/themes/chili/

Lilyum için SDDM teması içerir.

/usr/share/wallpapers/

Lilyum için duvar kağıtlarını içerir. (wallpaper-branding-openSUSE ve
breeze5-wallpapers paketleriyle ilişkilidir)

Dikkat: Bu duvar kağıtlarının lisansları kontrol edilmelidir, eğer
gerekliyse dosya isimleri resmin sahiplerine atfedilmelidir

Kullanıcı Dosyaları

Bu dosyalar /etc/skel, /root ve /home/lilyum dizinlerine eklenir.

/home/lilyum, kurulum ortamı kullanıcısı için gerekli ayarlamaları
içerir.

/root, kurulum sonrası ve kurulum esnasındaki yönetici için
yapılandırmaları içerir

/etc/skel ise kurulum sonrasındaki her kullanıcı için eklenecek
ayarlamaları içerir

Bu dosyalar içinde:

.config : Masaüstü yapılandırmaları ve ilgili temaları içerir

.kde ve .local/share : KDE için fazladan temaları ve renk şemalarını
içerir

.themes/Mc-OS-Transparent-1.3 : Lilyum'un GTK temasıdır.
